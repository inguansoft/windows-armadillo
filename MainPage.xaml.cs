﻿using System;
using System.Collections.ObjectModel;
using Windows.Foundation;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.Devices.Enumeration;
using System.Diagnostics;
using Armadillo.SensorTag;
using System.Collections.Generic;
using Armadillo.Service;
using Armadillo.DataModel;
using Windows.UI.Xaml.Media.Imaging;
using XamlAnimatedGif;

namespace Armadillo
{
    /// <summary>
    /// Sample app that communicates with Bluetooth device using the GATT profile
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public ObservableCollection<Device> DevicesLoadedCollection { get; private set; }
        public ObservableCollection<EventLogEntry> EventLogCollection { get; private set; }

        #region private members
        private TypedEventHandler<DeviceWatcher, DeviceInformation> handlerAdded = null;
        private TypedEventHandler<DeviceWatcher, DeviceInformationUpdate> handlerRemoved = null;
        private TypedEventHandler<DeviceWatcher, Object> handlerEnumCompleted = null;
        private DeviceWatcher bleWatcher = null;
        #endregion

        #region Constructor & Destructor
        public MainPage()
        {
            this.InitializeComponent();
            DevicesLoadedCollection = new ObservableCollection<Device>();
            EventLogCollection = new ObservableCollection<EventLogEntry>();

            StackContainerEvent.Visibility = Visibility.Collapsed;
            StackContainerDevice.Visibility = Visibility.Collapsed;
            StackContainerInit.Visibility = Visibility.Visible;
            EventLogComponent.Visibility = Visibility.Collapsed;

            DataContext = this;
            StartWatcher();
        }

        ~MainPage()
        {
            StopWatcher();
        }
        #endregion

        private void ensureDeviceIsInLoadedList(DeviceInformation deviceInfoDisp)
        {
            if (deviceInfoDisp != null)
            {
                foreach (Device devicePreLoaded in DevicesLoadedCollection)
                {
                    if (devicePreLoaded.GUID == deviceInfoDisp.Id)
                    {
                        return;
                    }
                }
                Device device = new Device(deviceInfoDisp);
                DevicesLoadedCollection.Add(device);
                if (deviceInfoDisp.Pairing.IsPaired)
                {
                    Magazine.Instance.ScheduleDevice(device);
                }
                device.DeviceChanged += c_UpdateUIDevice;
            }
            else {
                return;
            }
        }

        #region Watch on/off for BLE devices around
        /// <summary>
        /// Start Watcher to detect any changes on BLE devices available
        /// </summary>
        private void StartWatcher()
        {
            string aqsFilter = "System.Devices.Aep.ProtocolId:=\"{bb7bb05e-5972-42b5-94fc-76eaa7084d49}\"";
            string[] requestedProperties = { "System.Devices.Aep.IsPaired" };
            bleWatcher = DeviceInformation.CreateWatcher(aqsFilter, requestedProperties, DeviceInformationKind.AssociationEndpoint);

            #region handlers for searching BLE devices
            handlerAdded = new TypedEventHandler<DeviceWatcher, DeviceInformation>((watcher, deviceInfo) =>
           {
               if (deviceInfo != null && deviceInfo.Name != null &&
               deviceInfo.Name.Length > 0 && deviceInfo.Name.StartsWith("Armadillo"))
               {
                   ensureDeviceIsInLoadedList(deviceInfo);
               }
           });
            bleWatcher.Added += handlerAdded;

            //            handlerRemoved = new TypedEventHandler<DeviceWatcher, DeviceInformationUpdate>(async (watcher, deviceInfoUpdate) =>
            handlerRemoved = new TypedEventHandler<DeviceWatcher, DeviceInformationUpdate>((watcher, deviceInfoUpdate) =>
           {
               //if (deviceInfoUpdate.Id)
               //.Name.StartsWith("Armadillo"))
               //{
               //TODO: Dont plug just populate list as unplugged
               //}
               //await Dispatcher.RunAsync(CoreDispatcherPriority.Low, () =>
               //{
               // Find the corresponding DeviceInformation in the collection and remove it
               //  foreach (DeviceInformationDisplay deviceInfoDisp in ResultCollection)
               //{
               //  if (deviceInfoDisp.Id == deviceInfoUpdate.Id)
               //{
               //  ResultCollection.Remove(deviceInfoDisp);
               // break;
               //}
               //}
               //});

           });
            bleWatcher.Removed += handlerRemoved;

            #endregion

            bleWatcher.EnumerationCompleted += handlerEnumCompleted;
            bleWatcher.Start();
        }

        /// <summary>
        /// Stop Watcher to detect any changes on BLE devices available
        /// </summary>
        private void StopWatcher()
        {
            if (bleWatcher != null)
            {
                bleWatcher.Added -= handlerAdded;
                bleWatcher.Removed -= handlerRemoved;

                if (DeviceWatcherStatus.Started == bleWatcher.Status ||
                    DeviceWatcherStatus.EnumerationCompleted == bleWatcher.Status)
                {
                    bleWatcher.Stop();
                }
                bleWatcher = null;
            }
        }
        #endregion

        private void _updateLoadedDevicesList(DeviceChangedEventArgs e)
        {
            List<Device> cacheCollection = new List<Device>();
            foreach (Device devicePreLoaded in DevicesLoadedCollection)
            {
                cacheCollection.Add(devicePreLoaded);
            }
            Debug.WriteLine(e.device.GUID + ": " + e.PreviousMode.ToString() + "->" + e.CurrentMode.ToString());

            DevicesLoadedCollection = new ObservableCollection<Device>();
            //DevicesLoadedCollection.Clear();

            foreach (Device devicePreLoaded in cacheCollection)
            {
                DevicesLoadedCollection.Add(devicePreLoaded);
            }
            devicesLoaded.ItemsSource = DevicesLoadedCollection;
        }

        private async void c_UpdateUIDevice(object sender, DeviceChangedEventArgs e)
        {
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Low, () =>
            {
                _updateLoadedDevicesList(e);
            });
        }

        private void DevicesLoadedCollection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e != null && e.AddedItems != null && e.AddedItems.Count > 0)
            {
                Device dev = e.AddedItems[0] as Device;
                DetailsChosenTitle.Text = dev.Name;
                DeviceName.Text = dev.Name;
                DeviceGUID.Text = dev.GUID;

                if (dev.IsPaired)
                {
                    StackContainerEvent.Visibility = Visibility.Collapsed;
                    StackContainerDevice.Visibility = Visibility.Visible;
                    StackContainerInit.Visibility = Visibility.Collapsed;
                    EventLogComponent.Visibility = Visibility.Visible;
                    _fetchFromDeviceSelected(dev);

                    TimeOnFirstEventOnDevice.Text = dev.eventLog.TimeOnFirstEventOnDevice;
                    DeviceDuration.Text = dev.eventLog.DeviceDuration;
                }
                else {
                    Magazine.Instance.ScheduleDevice(dev);
                }
            }
        }

        private void _fetchFromDeviceSelected(Device device)
        {
            device.processBufferedEventLogReception();
            EventLogCollection.Clear();
            List<int> eventIndexes = new List<int>(device.eventLog.recognizedEvents.Keys);
            foreach (int eventIndex in eventIndexes)
            {
                EventLogCollection.Add(device.eventLog.recognizedEvents[eventIndex]);
            }
        }

        private void EventLogCollection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e != null && e.AddedItems != null && e.AddedItems.Count > 0)
            {
                StackContainerEvent.Visibility = Visibility.Visible;
                StackContainerDevice.Visibility = Visibility.Collapsed;
                StackContainerInit.Visibility = Visibility.Collapsed;
                EventLogEntry eventLog = e.AddedItems[0] as EventLogEntry;
                DetailsChosenTitle.Text = eventLog.EventType;
                DeviceNameFromMessage.Text = "from " + eventLog.Device.UniqueName;

                String uriString = "ms-appx:///Assets/" + eventLog.Type + ".gif";
                AnimationBehavior.SetSourceUri(DetailsChosenImage, new Uri(uriString));
                //0 "",
                //1 "Bend at waist",
                //2 "Left twist",
                //3 "Right twist",
                //4 "Left twist and Bend",
                //5 "Right twist and Bend",
                //6 "Left  side bend",
                //7 "Right side bend",
                //8 "Slouching"
                EventStartTimeStamp.Text = eventLog.Device.eventLog.eventStartTimeStamp(eventLog.Count);
                EventDuration.Text = eventLog.Device.eventLog.eventDuration(eventLog.Count);
            }
        }

    }
}
