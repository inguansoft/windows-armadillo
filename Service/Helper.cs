﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Armadillo.Service
{
    class Helper
    {
        #region Singleton constructor
        private Helper()
        {
        }
        private static Helper instance;
        public static Helper Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Helper();
                }
                return instance;
            }
        }
        #endregion

        public String stringFromDate(DateTime date) {
            return date.ToString("yyyy/MM/dd HH:mm:ss");
        }

        public Double getCoreVector(Double x, Double y, Double z)
        {
            return Math.Sqrt(((x * x) + (y * y)) + (z * z));
        }

        public String stringDuration(DateTime dateInit, DateTime dateEnd, string units) {
            TimeSpan difference = dateEnd - dateInit;
            int diffSecs = difference.Seconds, hrs;

            if (diffSecs <= 0)
            {
                return "Instant";
            }
            else {
                if (units == "hr") {
                    hrs = difference.Hours;
                    return hrs + " hours";
                }
                return diffSecs + " secs";
            }
        }


    }
}
