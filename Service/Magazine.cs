﻿using Armadillo.SensorTag;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Armadillo.Service
{
    class Magazine
    {
        #region Singleton constructor
        private Magazine()
        {
        }
        private static Magazine instance;
        public static Magazine Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Magazine();
                }
                return instance;
            }
        }
        #endregion

        public void ScheduleDevice(Device device) {
            if (device != null)
            {
                device.pair();
            }
        }

    }
}
