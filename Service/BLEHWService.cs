﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Storage.Streams;

namespace Armadillo.Service
{
    class BLEHWService
    {
        #region Singleton constructor
        private BLEHWService()
        {
        }
        private static BLEHWService instance;
        public static BLEHWService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BLEHWService();
                }
                return instance;
            }
        }
        #endregion

        public readonly String SERVICE_GUID = "0000ee00-0000-1000-8000-00805f9b34fb";

        public const int PLUGGED_UNKNOWN_STATE_MODE = -1,
            UNPLUGGED_MODE = 0,
            STREAM_MODE = 1,
            MONITORING_EVENT_MODE = 2,
            CALIBRATION_MODE = 3,
            EVENT_DOWNLOAD_MODE = 4,
            DELETE_EVENTS_MODE = 5,
            STANDBY_MODE = 6,
            FULL_EVENT_DOWNLOAD_MODE = 7;

        private const Double SECS_BASE = 1449187200;
        //Epoch timestamp:  1449187200   <--- in secs
        //Timestamp in milliseconds: 1449187200000
        //Human time (GMT): Fri, 04 Dec 2015 00:00:00 GMT

        public readonly Dictionary<int, String> EVENT_MODE_MAP = new Dictionary<int, String> {
            { PLUGGED_UNKNOWN_STATE_MODE, "waiting..."},
            { UNPLUGGED_MODE, "unplugged"},
            { STREAM_MODE, "Streaming"},
            { MONITORING_EVENT_MODE, "Monitoring"},
            { CALIBRATION_MODE, "Calibrating"},
            { EVENT_DOWNLOAD_MODE, "Event Download"},
            { DELETE_EVENTS_MODE, "Deleting Events on device"},
            { STANDBY_MODE, "Standby"},
            { FULL_EVENT_DOWNLOAD_MODE, "Full Event Download"}
        };

        public readonly string[] EVENT_TYPE_MAP = {
            "",
            "Bend at waist",
            "Left twist",
            "Right twist",
            "Left twist and Bend",
            "Right twist and Bend",
            "Left  side bend",
            "Right side bend",
            "Slouching"
        };

        public String getType(int typeVal)
        {
            if (typeVal > 0 && typeVal < 9)
            {
                return EVENT_TYPE_MAP[typeVal];
            }
            else {
                return "unknown(" + typeVal + ")";
            }
        }

        public DateTime getDate(int decSecs)
        {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(decSecs / 10 + SECS_BASE);
        }

        public int getCurrentTimeForDevice()
        {
            DateTime armadilloBase = new DateTime(2015, 12, 4, 0, 0, 0, DateTimeKind.Utc),
                today = DateTime.UtcNow;
            TimeSpan diff = today - armadilloBase;
            return (int)diff.TotalSeconds;
        }

        #region mode evals        
        public bool isSTREAM_MODE(int value)
        {
            return value == STREAM_MODE;
        }

        public bool isMONITORING_EVENT_MODE(int value)
        {
            return value == MONITORING_EVENT_MODE;
        }

        public bool isCALIBRATION_MODE(int value)
        {
            return value == CALIBRATION_MODE;
        }

        public bool isEVENT_DOWNLOAD_MODE(int value)
        {
            return value == EVENT_DOWNLOAD_MODE;
        }

        public bool isDELETE_EVENTS_MODE(int value)
        {
            return value == DELETE_EVENTS_MODE;
        }

        public bool isSTANDBY_MODE(int value)
        {
            return value == STANDBY_MODE;
        }

        public bool isFULL_EVENT_DOWNLOAD_MODE(int value)
        {
            return value == FULL_EVENT_DOWNLOAD_MODE;
        }
        #endregion

    }
}
