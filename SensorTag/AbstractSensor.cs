﻿using System;
using Windows.Devices.Bluetooth.GenericAttributeProfile;

namespace Armadillo.SensorTag
{
    public class AbstractSensor
    {
        protected String uuid;
        protected GattCharacteristic characteristic;

        public AbstractSensor(String uuidValue, GattCharacteristic characteristicValue)
        {
            this.uuid = uuidValue;
            this.characteristic = characteristicValue;
        }

        protected int getRTCValue(byte[] stream, int index)
        {
            int value = 0, exponent;
            
            for (int i = 0; i < 4; i++)
            {
                exponent = 1 << (i * 8);
                value = value + (stream[index - i]) * exponent;
            }
            return value;
        }

        protected int get16(byte[] stream, int index)
        {
            byte[] buffer = new byte[2] { stream[index+1], stream[index] };

            int result = BitConverter.ToInt16(buffer, 0);
            return result;
        }

        protected int[] getArrayValues(byte[] stream, int index, String label)
        {
            int[] result = new int[3] { 0, 0, 0 };
            result[0] = get16(stream, index);
            result[1] = get16(stream, index + 2);
            result[2] = get16(stream, index + 4);
            //print("\(label)[\(result[0]), \(result[1]), \(result[2])]")
            return result;
        }

    }
}
