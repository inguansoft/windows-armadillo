﻿using Armadillo.Service;
using System;
using System.Diagnostics;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Storage.Streams;

namespace Armadillo.SensorTag
{
    public class ModeSensor : AbstractSensor
    {
        private Device deviceAttached;
        public ModeSensor(String uuidValue, GattCharacteristic characteristicValue, Device device)
        : base(uuidValue, characteristicValue)
        {
            deviceAttached = device;
        }

        public void registerListener()
        {
            characteristic.ValueChanged += modeChanged;
        }

        public void unregisterListener() {
            characteristic.ValueChanged -= modeChanged;
        }

        private void modeChanged(
            GattCharacteristic sender, 
            GattValueChangedEventArgs eventArgs)
        {
            byte[] bArray = new byte[eventArgs.CharacteristicValue.Length];
            DataReader.FromBuffer(eventArgs.CharacteristicValue).ReadBytes(bArray);

            if (BLEHWService.Instance.isFULL_EVENT_DOWNLOAD_MODE(deviceAttached.CurrentMode) &&
                BLEHWService.Instance.isMONITORING_EVENT_MODE(bArray[0]))
            {
                deviceAttached.unpair();
            }
            else {
                deviceAttached.CurrentMode = bArray[0];
                Debug.WriteLine("----------------------------------- Mode Changed to: " +
                    BLEHWService.Instance.EVENT_MODE_MAP[bArray[0]]);
            }
        }
    }
}
