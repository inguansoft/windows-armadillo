﻿using System;
using System.Collections.Generic;
using Windows.Foundation;
using Windows.UI.Core;
using Windows.Devices.Enumeration;
using System.Diagnostics;
using Armadillo.Service;

using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Storage.Streams;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Armadillo.DataModel;

namespace Armadillo.SensorTag
{
    public class Device
    {
        #region private members
        private Dictionary<String, AbstractSensor> gateways = null;
        private DeviceInformation bleDeviceInfo;
        private DeviceWatcher bleWatcher = null;
        private TypedEventHandler<DeviceWatcher, DeviceInformation> OnBLEAdded = null;
        //private TypedEventHandler<DeviceWatcher, DeviceInformationUpdate> OnBLEUpdated = null;
        //private TypedEventHandler<DeviceWatcher, DeviceInformationUpdate> OnBLERemoved = null;
        #endregion

        #region constructor and destructor
        public Device(DeviceInformation deviceInfo)
        {
            bleDeviceInfo = deviceInfo;
            this.gateways = new Dictionary<string, AbstractSensor>();
            eventLog = new EventLog();
        }

        ~Device()
        {
            unplug();
        }
        #endregion

        #region attributes with accessors
        public EventLog eventLog;

        private int currentMode = BLEHWService.PLUGGED_UNKNOWN_STATE_MODE;
        public String GUID
        {
            get
            {
                if (bleDeviceInfo.Id.Length > 35)
                {
                    return bleDeviceInfo.Id.Substring(bleDeviceInfo.Id.Length - 35);
                }
                else {
                    return bleDeviceInfo.Id;
                }
            }
        }

        public event EventHandler<DeviceChangedEventArgs> DeviceChanged;
        protected virtual void OnDeviceChanged(DeviceChangedEventArgs e)
        {
            EventHandler<DeviceChangedEventArgs> handler = DeviceChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public int CurrentMode
        {
            get
            {
                return currentMode;
            }

            set
            {
                DeviceChangedEventArgs args = new DeviceChangedEventArgs();
                args.device = this;
                args.PreviousMode = currentMode;
                args.CurrentMode = value;
                currentMode = value;
                CurrentModeName = BLEHWService.Instance.EVENT_MODE_MAP[currentMode];
                OnDeviceChanged(args);

                if (args.PreviousMode == BLEHWService.EVENT_DOWNLOAD_MODE &&
                    (args.CurrentMode == BLEHWService.MONITORING_EVENT_MODE ||
                    args.CurrentMode == BLEHWService.UNPLUGGED_MODE))
                {
                    processBufferedEventLogReception();
                    //e.device.unplug();
                }
            }
        }

        public String UniqueName
        {
            get
            {
                if (bleDeviceInfo.Id.Length > 11)
                {
                    return Name + "(" + bleDeviceInfo.Id.Substring(bleDeviceInfo.Id.Length - 11) + ")";
                }
                else {
                    return Name + "(" + bleDeviceInfo.Id + ")";
                }
            }
        }

        public bool IsPaired {
            get {
                return bleDeviceInfo.Pairing.IsPaired;
            }
        }
        #endregion

        #region UI attr
        private String currentModeName = "Unplugged";
        public String CurrentModeName
        {
            get
            {
                return currentModeName;
            }
            set
            {
                currentModeName = value;
            }
        }

        public String Name
        {
            get
            {
                return bleDeviceInfo.Name;
            }
        }

        #endregion

        public GattCharacteristic deviceModeCharacteristic,
            bucketCharacteristic,
            eventLogCharacteristic,
            timeCharacteristic;
        public ModeSensor modeSensor;
        public AccelerationSensor aulSensor;
        public EventLogFromSensor eventLogFromSensor;

        public void processBufferedEventLogReception()
        {
            eventLogFromSensor.processBufferedReception();
        }

        public async void setCurrentTimeOnDevice()
        {
            DataWriter writer = new DataWriter();
            int currentTime = BLEHWService.Instance.getCurrentTimeForDevice();
            //writer.WriteUInt32(currentTime);
            Debug.WriteLine("CurrentTime:" + currentTime);

            byte[] currentTimeBytes = BitConverter.GetBytes(currentTime);
            Array.Reverse(currentTimeBytes);

            writer.WriteBytes(currentTimeBytes);

            await timeCharacteristic.
                WriteValueAsync(writer.DetachBuffer());
        }

        private async void _abstractModeSend(int modeValue)
        {
            DataWriter writer = new DataWriter();
            writer.WriteByte((byte)modeValue);
            GattCommunicationStatus status =
                await deviceModeCharacteristic.WriteValueAsync(
                    writer.DetachBuffer());
        }

        public void setFullDownloadMode()
        {
            _abstractModeSend(BLEHWService.FULL_EVENT_DOWNLOAD_MODE);
        }

        public void setDownloadMode()
        {
            _abstractModeSend(BLEHWService.EVENT_DOWNLOAD_MODE);
        }

        public void setDeleteEventsMode()
        {
            _abstractModeSend(BLEHWService.DELETE_EVENTS_MODE);
        }

        //            Upper Left Sensor Acceleration	0xEE10
        //            Upper Left Sensor Gyro	0xEE11
        //            Upper Right Sensor Acceleration	0xEE20
        //            Upper Right Sensor Gyro	0xEE21
        //            Middle Sensor Acceleration	0xEE30
        //            Middle Sensor Gyro	0xEE31
        //            Lower Sensor Acceleration	0xEE40
        //            Lower Sensor Gyro	0xEE41
        //            Device Mode	0xEE50
        //            RTC Time	0xEE70
        //            Event Entry	0xEE60

        private async void _enableNotificationOnCharacteristic(GattCharacteristic characteristic)
        {
            if (characteristic.CharacteristicProperties.HasFlag(
                                  GattCharacteristicProperties.Notify))
            {
                await characteristic.WriteClientCharacteristicConfigurationDescriptorAsync(
                    GattClientCharacteristicConfigurationDescriptorValue.Notify);
            }
        }

        /// <summary>
        /// Check if the characteristic has a valid data UUID
        /// </summary>
        /// <param name="characteristic">charcateristic value</param>
        public void registerCharacteristic(GattCharacteristic characteristic)
        {
            Guid Guuid = characteristic.Uuid;
            String uuid = Guuid.ToString();
            switch (uuid)
            {
                case "0000ee10-0000-1000-8000-00805f9b34fb":
                    aulSensor = new AccelerationSensor(uuid, characteristic);
                    gateways.Add(uuid, aulSensor);
                    break;
                case "0000ee20-0000-1000-8000-00805f9b34fb":
                case "0000ee30-0000-1000-8000-00805f9b34fb":
                case "0000ee40-0000-1000-8000-00805f9b34fb":
                    gateways.Add(uuid, new AccelerationSensor(uuid, characteristic));
                    break;
                case "0000ee11-0000-1000-8000-00805f9b34fb":
                case "0000ee21-0000-1000-8000-00805f9b34fb":
                case "0000ee31-0000-1000-8000-00805f9b34fb":
                case "0000ee41-0000-1000-8000-00805f9b34fb":
                    gateways.Add(uuid, new GyroSensor(uuid, characteristic));
                    break;
                case "0000ee50-0000-1000-8000-00805f9b34fb": //Mode Characteristic
                    _enableNotificationOnCharacteristic(characteristic);

                    deviceModeCharacteristic = characteristic;
                    modeSensor = new ModeSensor(uuid, characteristic, this);
                    modeSensor.registerListener();
                    gateways.Add(uuid, modeSensor);
                    break;
                case "0000ee60-0000-1000-8000-00805f9b34fb": //Event logs download
                    _enableNotificationOnCharacteristic(characteristic);

                    eventLogCharacteristic = characteristic;
                    eventLogFromSensor = new EventLogFromSensor(uuid, characteristic, eventLog, this);
                    eventLogFromSensor.registerListener();
                    gateways.Add(uuid, eventLogFromSensor);
                    break;
                case "0000ee70-0000-1000-8000-00805f9b34fb": //Time
                    timeCharacteristic = characteristic;
                    break;
                default:
                    bucketCharacteristic = characteristic;
                    break;
            }
        }

        public void initEventDownloadBuffer()
        {
            eventLogFromSensor.initBuffer();
        }

        #region pairing & unpairing
        public async void pair()
        {
            if (!bleDeviceInfo.Pairing.IsPaired)
            {
                DeviceInformationCustomPairing customPairing = bleDeviceInfo.Pairing.Custom;

                customPairing.PairingRequested += PairingRequestedHandler;
                DevicePairingResult result = await customPairing.PairAsync(DevicePairingKinds.ConfirmOnly,
                    DevicePairingProtectionLevel.Default);
                customPairing.PairingRequested -= PairingRequestedHandler;

                if (result.Status == DevicePairingResultStatus.Paired)
                {
                    StartBLEWatcher();
                }
                else {
                    Debug.WriteLine("Pairing Failed " + result.Status.ToString());
                }
            }
            else {
                StartBLEWatcher();
            }
        }

        private void PairingRequestedHandler(
            DeviceInformationCustomPairing sender,
            DevicePairingRequestedEventArgs args)
        {
            switch (args.PairingKind)
            {
                case DevicePairingKinds.ConfirmOnly:
                    // Windows itself will pop the confirmation dialog as part of "consent" if this is running on Desktop or Mobile
                    // If this is an App for 'Windows IoT Core' where there is no Windows Consent UX, you may want to provide your own confirmation.
                    args.Accept();
                    break;
                default:
                    Debug.WriteLine("What is this: " + args.PairingKind);
                    args.Accept();
                    break;
            }
        }

        /// <summary>
        /// Unplug device
        /// </summary>
        public void unplug()
        {
            CurrentMode = BLEHWService.UNPLUGGED_MODE;
            gateways.Clear();
            eventLogFromSensor.unregisterListener();
            modeSensor.unregisterListener();
        }

        public async void unpair()
        {
            Debug.WriteLine("Unplugging...");
            if (bleDeviceInfo == null)
            {
                Debug.WriteLine("Unexpected unpair with null bleDeviceInfo");
            }
            else
            {
                unplug();
                DeviceUnpairingResult dupr = await bleDeviceInfo.Pairing.UnpairAsync();
                bleDeviceInfo = null;
                Debug.WriteLine("Unplugged!");
            }
        }
        #endregion

        #region Watch on/off for connected BLE device activity
        private async Task<GattDeviceService> init()
        {
            Guid BLE_GUID = new Guid(BLEHWService.Instance.SERVICE_GUID);
            var services = await DeviceInformation.FindAllAsync(
                GattDeviceService.GetDeviceSelectorFromUuid(BLE_GUID), null);
            if (services != null && services.Count > 0)
            {
                if (services[0].IsEnabled)
                {
                    GattDeviceService service = await GattDeviceService.FromIdAsync(services[0].Id);
                    return service;
                }
            }
            return null;
        }

        private void _setupDeviceCharacteristics(
          GattDeviceService coreService)
        {
            if (coreService == null)
            {
                Debug.WriteLine("Need a service to enable Device");
            }
            else {
                IReadOnlyList<GattCharacteristic> characteristicList;
                characteristicList = coreService.GetAllCharacteristics();
                if (characteristicList != null)
                {
                    foreach (GattCharacteristic characteristic in characteristicList)
                    {
                        registerCharacteristic(characteristic);
                    }
                }
            }
        }

        private void _retrieveFullEventLog()
        {
            setCurrentTimeOnDevice();
            setFullDownloadMode();
        }

        private void _retrieveEventLog()
        {
            setCurrentTimeOnDevice();
            setDownloadMode();
        }

        private void StartBLEWatcher()
        {
            OnBLEAdded = new TypedEventHandler<DeviceWatcher, DeviceInformation>(async (watcher, deviceInfo) =>
            {
                //await Dispatcher.RunAsync(CoreDispatcherPriority.Low, async () =>
                //{
                GattDeviceService service = await init();
                if (service == null)
                {
                    Debug.WriteLine("Something went wrong on StartBLEWatcher");
                }
                else
                {
                    _setupDeviceCharacteristics(service);
                    //_retrieveFullEventLog();
                    _retrieveEventLog();
                }
                StopBLEWatcher();
                //});
            });

            //OnBLEUpdated = new TypedEventHandler<DeviceWatcher, DeviceInformationUpdate>(async (watcher, deviceInfoUpdate) =>
            //{
            //    await Dispatcher.RunAsync(CoreDispatcherPriority.Low, () =>
            //    {
            //        Debug.WriteLine("OnUpdated");
            //        Debug.WriteLine(deviceInfoUpdate.Id);
            //    });
            //});


            //OnBLERemoved = new TypedEventHandler<DeviceWatcher, DeviceInformationUpdate>(async (watcher, deviceInfoUpdate) =>
            //{
            //    await Dispatcher.RunAsync(CoreDispatcherPriority.Low, () =>
            //    {
            //        Debug.WriteLine("OnRemoved");

            //    });
            //});

            bleWatcher = DeviceInformation.CreateWatcher(GattDeviceService.GetDeviceSelectorFromUuid(GattServiceUuids.GenericAccess));
            bleWatcher.Added += OnBLEAdded;
            //bleWatcher.Updated += OnBLEUpdated;
            //bleWatcher.Removed += OnBLERemoved;
            bleWatcher.Start();
        }

        private void StopBLEWatcher()
        {
            if (bleWatcher == null)
            {
                Debug.WriteLine("Not able to stop null Watcher!");
            }
            else
            {
                bleWatcher.Added -= OnBLEAdded;
                //bleWatcher.Updated -= OnBLEUpdated;
                //bleWatcher.Removed -= OnBLERemoved;

                if (DeviceWatcherStatus.Started == bleWatcher.Status ||
                    DeviceWatcherStatus.EnumerationCompleted == bleWatcher.Status)
                {
                    bleWatcher.Stop();
                }
                bleWatcher = null;
            }
        }
        #endregion

    }

    public class DeviceChangedEventArgs : EventArgs
    {
        public Device device { get; set; }
        public int PreviousMode { get; set; }
        public int CurrentMode { get; set; }
    }

}
