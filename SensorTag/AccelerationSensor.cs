﻿using System;
using System.Diagnostics;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Storage.Streams;

namespace Armadillo.SensorTag
{
    public class AccelerationSensor : AbstractSensor
    {
        public AccelerationSensor(String uuidValue, GattCharacteristic characteristicValue)
        :base(uuidValue, characteristicValue){
            //characteristicValue.ValueChanged += accelChanged;
        }

        // Accelerometer change handler
        // Algorithm taken from http://processors.wiki.ti.com/index.php/SensorTag_User_Guide#Accelerometer_2
        void accelChanged(GattCharacteristic sender, GattValueChangedEventArgs eventArgs)
        {
            byte[] bArray = new byte[eventArgs.CharacteristicValue.Length];
            DataReader.FromBuffer(eventArgs.CharacteristicValue).ReadBytes(bArray);
            //Debug.WriteLine("DATA ACC:");
            //Debug.WriteLine(bArray);
            double x = (SByte)bArray[0] / 64.0;
            double y = (SByte)bArray[1] / 64.0;
            double z = (SByte)bArray[2] / 64.0 * -1;
        }

    }
}
