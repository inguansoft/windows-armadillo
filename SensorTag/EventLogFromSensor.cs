﻿using System;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Storage.Streams;
using Armadillo.DataModel;
using System.Diagnostics;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Armadillo.SensorTag
{
    public class EventLogFromSensor : AbstractSensor
    {
        private List<IBuffer> buffer;
        private int streamsReceivedCounter = 0;
        private int bufferCounter, errorCounter;
        private EventLogEntry eventlogEntry;
        private EventLog _eventLog;
        private Device _device;

        public void initBuffer()
        {
            bufferCounter = 0;
            eventlogEntry = new EventLogEntry(_device);
        }

        public EventLogFromSensor(String uuidValue, GattCharacteristic characteristicValue, EventLog eventLog, Device device)
        : base(uuidValue, characteristicValue)
        {
            _device = device;
            bufferCounter = 0;
            errorCounter = 0;
            streamsReceivedCounter = 0;
            initBuffer();
            buffer = new List<IBuffer>();
            _eventLog = eventLog;
            _eventLog.clear();
        }

        public void registerListener()
        {
            characteristic.ValueChanged += eventLogChanged;
        }


        public void unregisterListener()
        {
            characteristic.ValueChanged -= eventLogChanged;
        }

        private Boolean _setStream(Byte[] stream)
        {
            streamsReceivedCounter++;
            //String streamReport = "";
            bufferCounter++;
            //for (int i = 0; i < stream.Length; i++)
            //{
            //    streamReport += stream[i].ToString("X2");
            //}
            //Debug.WriteLine(":::" + stream[0]);
            if (bufferCounter == stream[0])
            {
                return _loadToModel(stream);
            }
            else {
                errorCounter++;
                if (bufferCounter > 1)
                {
                    _eventLog.add(eventlogEntry);
                }
                Debug.WriteLine(" -------------------------------- expected:" + bufferCounter +" actual:" + stream[0]);
                if (stream[0] == 1)
                {
                    //Debug.WriteLine("Trying to correct at index 1, just throwing errand and trying to catch from scratch");
                    initBuffer();
                    bufferCounter++;
                    return _loadToModel(stream);
                }
                else {
                    return false;
                }
            }
        }

        private void _process(byte[] stream)
        {
            if (!_setStream(stream))
            {
                _errorProtocol(stream);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void eventLogChanged(GattCharacteristic sender, GattValueChangedEventArgs eventArgs)
        {
            buffer.Add(eventArgs.CharacteristicValue);
        }

        public void processBufferedReception()
        {
            foreach (IBuffer buff in buffer)
            {
                byte[] bArray = new byte[buff.Length];
                DataReader.FromBuffer(buff).ReadBytes(bArray);
                //Debug.WriteLine("received(" + buff.Length + "):" + bArray[0]);
                _process(bArray);
                if (bufferCounter == 3)
                {
                    _eventLog.add(eventlogEntry);
                    initBuffer();
                }
            }
            Debug.WriteLine("Streams received:" + streamsReceivedCounter);
            Debug.WriteLine("Errors:" + errorCounter);
            Debug.WriteLine("EventLogEntries:" + _eventLog.items.Count);
            buffer.Clear();
            streamsReceivedCounter = 0;
            errorCounter = 0;
        }

        private void _errorProtocol(byte[] stream)
        {
            //Debug.WriteLine("Error! expected " + bufferCounter + "!=" + stream[0] + " received -> Init set to = 1");
            initBuffer();
        }

        #region loadToModel methods
        private Boolean _loadToModel1(byte[] stream)
        {
            int rtcTime = getRTCValue(stream, 7);
            int[] xyz = new int[3];

            eventlogEntry.Count = get16(stream, index: 1);
            //print("Count(\(stream[1])+\(stream[2]))=\(value)")

            eventlogEntry.Type = (int)stream[3];
            //print("EventType: \(stream[3])")

            eventlogEntry.time = new EventLogTime();
            eventlogEntry.time.value = rtcTime;

            xyz = getArrayValues(stream, 8, "aul");
            eventlogEntry.aul = new SensorXYZ();
            eventlogEntry.aul.x = xyz[0];
            eventlogEntry.aul.y = xyz[1];
            eventlogEntry.aul.z = xyz[2];
            //        print("Acc: \(stream[8]):\(stream[9]):\(stream[10]):\(stream[11]):\(stream[12]):\(stream[13])")

            xyz = getArrayValues(stream, index: 14, label: "gul");
            eventlogEntry.gul = new SensorXYZ();
            eventlogEntry.gul.x = xyz[0];
            eventlogEntry.gul.y = xyz[1];
            eventlogEntry.gul.z = xyz[2];
            //        print("Gyro: \(stream[14]):\(stream[15]):\(stream[16]):\(stream[17]):\(stream[18]):\(stream[19])")
            return true;
        }

        private Boolean _loadToModel2(byte[] stream)
        {
            int[] xyz = new int[3];

            xyz = getArrayValues(stream, 1, "amc");
            eventlogEntry.amc = new SensorXYZ();
            eventlogEntry.amc.x = xyz[0];
            eventlogEntry.amc.y = xyz[1];
            eventlogEntry.amc.z = xyz[2];

            xyz = getArrayValues(stream, 7, "gmc");
            eventlogEntry.gmc = new SensorXYZ();
            eventlogEntry.gmc.x = xyz[0];
            eventlogEntry.gmc.y = xyz[1];
            eventlogEntry.gmc.z = xyz[2];
            return true;
        }

        private Boolean _loadToModel3(byte[] stream)
        {
            int[] xyz = getArrayValues(stream, 1, "alc");

            eventlogEntry.alc = new SensorXYZ();
            eventlogEntry.alc.x = xyz[0];
            eventlogEntry.alc.y = xyz[1];
            eventlogEntry.alc.z = xyz[2];

            xyz = getArrayValues(stream, 7, "glc");
            eventlogEntry.glc = new SensorXYZ();
            eventlogEntry.glc.x = xyz[0];
            eventlogEntry.glc.y = xyz[1];
            eventlogEntry.glc.z = xyz[2];
            return true;
        }

        private Boolean _loadToModel(byte[] stream)
        {
            switch (stream[0])
            {
                case 1:
                    if (stream.Length == 20)
                    {
                        return _loadToModel1(stream);
                    }
                    else {
                        Debug.WriteLine("Unexpected stream size on 1 expected 20 received: " + stream.Length.ToString());
                        return false;
                    }
                case 2:
                    if (stream.Length == 13)
                    {
                        return _loadToModel2(stream);
                    }
                    else {
                        Debug.WriteLine("Unexpected stream size on 2 expected 13 received: " + stream.Length.ToString());
                        return false;
                    }
                case 3:
                    if (stream.Length == 13)
                    {
                        return _loadToModel3(stream);
                    }
                    else {
                        Debug.WriteLine("Unexpected stream size on 3 expected 13 received: " + stream.Length.ToString());
                        return false;
                    }
                default:
                    Debug.WriteLine("Unexpected stream #" + stream[0]);
                    return false;
            }
        }
        #endregion

    }
}
