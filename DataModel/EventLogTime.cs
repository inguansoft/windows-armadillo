﻿using System;
using Armadillo.Service;
using System.Diagnostics;

namespace Armadillo.DataModel
{
    public class EventLogTime
    {
        public int value;

        public String report() {
            DateTime dateObj = BLEHWService.Instance.getDate(value);
            return Helper.Instance.stringFromDate(dateObj);
        }
    }
}
