﻿using Armadillo.Service;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Armadillo.DataModel
{
    public class EventLog
    {
        public Dictionary<int, EventLogEntry> recognizedEvents;

        public EventLog()
        {
            items = new List<EventLogEntry>();
            recognizedEvents = new Dictionary<int, EventLogEntry>();
        }

        public List<EventLogEntry> items;

        public void clear()
        {
            items.Clear();
            recognizedEvents.Clear();
        }

        public EventLogEntry get(int index)
        {
            if (items.Count > index)
            {
                return items[index];
            }
            else {
                return null;
            }
        }

        public void add(EventLogEntry item)
        {
            items.Add(item);
            if (item.time.value > _max) {
                _max = item.time.value;
            }
            if (item.time.value < _min || _min == 0)
            {
                _min = item.time.value;
            }
            if (recognizedEvents.ContainsKey(item.Count))
            {
                recognizedEvents[item.Count].grabMetrics(item);
            }
            else
            {
                recognizedEvents.Add(item.Count, item);
                recognizedEvents[item.Count].grabMetrics(item);
                //--------Debug.WriteLine(">>>" + item.count + "(" + BLEHWService.Instance.getType(item.type) + ")");
            }
        }


        public string eventStartTimeStamp(int count)
        {
            return recognizedEvents[count].EventStartTimeStamp;
        }

        public string eventDuration(int count)
        {
            return recognizedEvents[count].EventDuration;
        }

        private int _min = 0, _max =0;

        public string TimeOnFirstEventOnDevice
        {
            get
            {
                DateTime dateObj = BLEHWService.Instance.getDate(_min);
                return Helper.Instance.stringFromDate(dateObj);
            }
        }

        public string DeviceDuration
        {
            get
            {
                DateTime dateInit = BLEHWService.Instance.getDate(_min),
                    dateEnd = BLEHWService.Instance.getDate(_max);
                return Helper.Instance.stringDuration(dateInit, dateEnd, "hr");
            }
        }

    }
}