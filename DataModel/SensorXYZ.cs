﻿using Armadillo.Service;
using System;

namespace Armadillo.DataModel
{
    public class SensorXYZ
    {
        public int x, y, z;

        public String report()
        {
            Double vector = Helper.Instance.getCoreVector(x, y, z);
            return String.Format("{0:0.00}", vector / 1000);
        }

    }
}
