﻿using Armadillo.SensorTag;
using Armadillo.Service;
using System;

namespace Armadillo.DataModel
{
    public class EventLogEntry
    {

        public EventLogEntry(Device dev)
        {
            device = dev;
            count = 0;
            time = new EventLogTime();
            type = 0;

            aul = new SensorXYZ();
            gul = new SensorXYZ();
            aur = new SensorXYZ();
            gur = new SensorXYZ();
            amc = new SensorXYZ();
            gmc = new SensorXYZ();
            alc = new SensorXYZ();
            glc = new SensorXYZ();
        }

        private Device device;
        private int count, type;

        public int Count
        {
            get
            {
                return count;
            }
            set
            {
                count = value;
            }
        }

        public Device Device
        {
            get
            {
                return device;
            }
        }

        public int Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        public String TimeStamp
        {
            get
            {
                return time.report();
            }
        }

        public String EventType
        {
            get
            {
                return BLEHWService.Instance.getType(type);
            }
        }

        public String Payload
        {
            get
            {
                return String.Format("{0:0.0}, {1:0.0}, {2:0.0}, {3:0.0}, {4:0.0}, {5:0.0}",
                    aul.report(),
                    gul.report(),
                    amc.report(),
                    gmc.report(),
                    alc.report(),
                    glc.report());
            }
        }

        public EventLogTime time;

        public SensorXYZ aul, gul, aur, gur, amc, gmc, alc, glc;

        public String report()
        {
            String result = String.Format(
                "{0:0}, {1}, {2}, {3:0.0}, {4:0.0}, {5:0.0}, {6:0.0}, {7:0.0}, {8:0.0}",
                count,
                time.report(),
                BLEHWService.Instance.getType(type),
                aul.report(),
                gul.report(),
                amc.report(),
                gmc.report(),
                alc.report(),
                glc.report());

            return result;
        }





        private int _min = 0, _max = 0;

        public void grabMetrics(EventLogEntry item)
        {
            if (item.time.value > _max)
            {
                _max = item.time.value;
            }
            if (item.time.value < _min || _min == 0)
            {
                _min = item.time.value;
            }
        }

        public String EventStartTimeStamp
        {
            get
            {
                DateTime dateObj = BLEHWService.Instance.getDate(_min);
                return Helper.Instance.stringFromDate(dateObj);
            }
        }

        public String EventDuration
        {
            get
            {
                DateTime dateInit = BLEHWService.Instance.getDate(_min),
                    dateEnd = BLEHWService.Instance.getDate(_max);
                return Helper.Instance.stringDuration(dateInit, dateEnd, "sec");
            }
        }

    }
}
